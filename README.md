# Evaluation Module WordPress IUT 2023

!!!!!!!!!! **Committez le + souvent possible** !!!!!!!!!!

## Préparation

1. Forkez ce dépôt (public)
2. Ajoutez moi comme moderateur : @nderambure (dw4y)
3. Depuis Gitlab, lancez Gitpod avec le bouton "Gitpod" sur l'accueil du dépôt
4. Attendre que Visual Code démarre avec les containers Docker
5. Dans Visual Code (VS), en bas a droite, cliquer sur "Ports: 8000" et dans la liste qui apparait, cliquer sur la planète
6. Installer WordPress : penser à aller changer les permaliens dans les réglages du site une fois connecté à l'admin
7. Tjs dans l'admin du site, menu Apparence > Thèmes, installer le thème "Twenty Nineteen"
8. Ouvrir un terminal dans VS, vous êtes à la racine, et executer cette commande pour donner les droits d'écriture sur le dossier wp-content/thème :

`sudo chown gitpod: wpdata/wp-content/themes`

## 1) Créer un thème enfant de Twenty Nineteen (5 points)

<a href="https://developer.wordpress.org/themes/advanced-topics/child-themes/" target="_blank">Doc création d'un thème enfant.</a>

1. Créer un dossier "iut" dans le dossier wp-content/themes/
2. Y créer les 2 fichiers obligatoires (un .php et un .css)
3. Dans le fichier CSS, mettre le bon entête pour déclarer les infos du thème, et le fait qu'il est enfant de Twenty Nineteen.
4. Dans le fichier PHP, ajouter la function qui charge le CSS du thème parent, ainsi que celui du thème enfant :

D'après la doc, la function que l'on crée dépend de comment le thème parent charge lui-même son fichier CSS : voir comment il le fait dans le fichier "functions.php" du thème Nineteen, ligne 263.

Penser à renommer les prefix "my_theme_" par "iut_".

Activer le thème iut dans l'admin de WordPress.

## 2) Créer un type de contenu "Recette" (5 points)

<a href="https://developer.wordpress.org/plugins/post-types/registering-custom-post-types/" target="_blank">Doc création d'un type de contenu.</a>

Le site est pour un restaurant qui veut afficher une liste de ses recettes.

Créer un type de contenu dont le slug sera "recipe" (en anglais) :

- ajouter le paramètre "show_in_rest" avec comme valeur "true" pour permettre au type de contenu d'utiliser l'editeur Gutenberg
- l'url de la page listant les recettes doit être "recettes" (paramètre "has_archive").
- l'url d'une page de recette doit être "recette" (paramètre "rewrite").

Pour voir votre type de contenu, il faut d'abord changer de thème et revenir au thème "iut" afin de recharger les droits dans WordPress.

## 3) Créer une metabox au type de contenu "Recette" (5 points)

<a href="https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/" target="_blank">Doc création d'une metabox.</a>

Créer une metabox dont le titre sera "Infos complémentaires", dans laquelle on affiche un champ "Ingrédients" (slug : "ingredients") sous forme de textarea.

Le champ doit être sauvé et retrouvé quand on revient dans le formulaire de recette.

## 4) Créer les fichiers de templates (5 points)

<a href="https://developer.wordpress.org/files/2014/10/Screenshot-2019-01-23-00.20.04.png" target="_blank">Doc Hiérarchie des templates</a>

D'après le document ci-dessus, créer dans le thème enfant :

- Un fichier qui s'occupera d'afficher le "single" d'une recette (et seulement d'une recette)
- Un fichier qui s'occupera d'afficher la liste des recettes (et seulement les recettes) (aussi appelée "archive")

Attention à copier les bons fichiers depuis le thème parent.

Si vous avez le temps, afficher la valeur du champ "ingredients" dans le single d'une recette (n'importe où, pas d'importance ici).

## 5) (BONUS) Créer une taxonomie "Saison" (2 points)

<a href="https://developer.wordpress.org/reference/functions/register_taxonomy/" target="_blank">Doc création d'une taxonomie.</a>

On devrait pouvoir ranger les recettes dans une taxonomie "Saison" dont le slug est "season".