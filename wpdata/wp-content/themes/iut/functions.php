<?php
function iut_wp_enqueue_scripts() {
	$parenthandle = 'twentynineteen-style';
	$theme        = wp_get_theme();

  // Load parent CSS
	wp_enqueue_style( 
    	$parenthandle,
		get_template_directory_uri() . '/style.css', // url fichier css du thème parent
		array(),
		$theme->parent()->get( 'Version' )
	);


  // Load CSS child
	wp_enqueue_style( 
    	'iut-style',
		get_stylesheet_uri(), // url fichier css du thème enfant
		array( $parenthandle ),
		$theme->get( 'Version' )
	);
}

add_action( 'wp_enqueue_scripts', 'iut_wp_enqueue_scripts' );


// Creer contenu recette
function iut_register_post_type_recipe()
{
  register_post_type(
    'recipe',
    array(
      'labels' => array(
        'name' => __('Recettes'),
        'singular_name' => __('Recette'),
      ),
      'public' => true,
      'has_archive' => 'recipe',
      'rewrite' => array('slug' => 'recipe'),
      'show_in_rest' => true
    )
  );
}
add_action('init', 'iut_register_post_type_recipe', 10);


/*** Création metabox ***/

function iut_add_meta_boxes_recipe($post) {
	add_meta_box(
		'iut_mbox_recipe',                 	// Unique ID
		'Infos complémentaires',      // Box title
		'iut_mbox_recipe_content',  			// Content callback, must be of type callable
		'recipe'                            	// Post type
	);
}
add_action('add_meta_boxes', 'iut_add_meta_boxes_recipe');

function iut_mbox_recipe_content($post) {
	// Get meta value
	$iut_ingredients = get_post_meta(
		$post->ID,
		'iut-ingredients',
		true
	);

	echo '
	<p>
		<label for="iut-ingredients">Ingrédients : </label>
		<textarea id="iut-ingredients" name="iut-ingredients" col="50" row="20">'.$iut_ingredients.'</textarea>
	</p>
	';
    var_dump($iut_ingredients);
}

// Save post meta
function iut_save_post($post_id) {
	if (isset($_POST['iut-ingredients']) && !empty($_POST['iut-ingredients'])) {
		update_post_meta(
			$post_id,
			'iut-ingredients',
			sanitize_textarea_field($_POST['iut-ingredients'])
		);
	}
}

add_action('save_post', 'iut_save_post');