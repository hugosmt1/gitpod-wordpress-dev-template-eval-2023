<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
				?>
			</header><!-- .page-header -->

			<?php
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content-recipe', 'recipe' );

			endwhile;

			twentynineteen_the_posts_navigation();

		else :
			get_template_part( 'template-parts/content/content', 'none' );

		endif;
		?>
		</main>
	</div>

<?php
get_footer();


/*
//BONUS
 CTX type de projet 
function iut_register_ctx_recettes_type() {
	register_taxonomy(
		'recipe-type',
		'recipe',
		array(
			'labels' 				=> array(
				'name' 			=> 'Catégories de recettes',
				'singular_name' => 'Catégorie de recettes'
			),
			'public' 				=> true, 	// false = cachée de l'interface d'admin et du frontend
			'rewrite' 				=> array('slug' => 'categories-de-recettes'),
		)
	);
}

add_action('init', 'iut_register_ctx_recettes_type', 11);
*/